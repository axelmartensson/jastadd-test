// AST NTAs are only cached if declared lazy.
import static runtime.Test.*;

public class Test {
	public static void main(String[] args) {
		A a = new A();

		// Non-lazy AST NTA is not cached.
		testNotSame(a.getB(), a.getB());
		
		// Lazy AST NTA is cached.
		testSame(a.getLazyB(), a.getLazyB());
	}
}
