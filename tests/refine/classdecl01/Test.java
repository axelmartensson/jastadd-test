import static runtime.Test.*;

public class Test {
	public static void main(String[] args) {
		testFalse(new Node().find("red"));
		testTrue(new Node().find("blue"));
	}
}
