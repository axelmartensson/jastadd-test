import static runtime.Test.*;

public class Test {
	public static void main(String[] args) {
		testEqual(16, new X().addSomething(3));
		testNull(new X().returnSomething(new ASTNode()));
	}
}
