// .result=JASTADD_ERR_OUTPUT
import static runtime.Test.*;

public class Test {
  public static void main(String[] args) {
    A a = new A();
    B b = new B();
    testFalse(a.attr());
    testTrue(b.attr());
  }
}
